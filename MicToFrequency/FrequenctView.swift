//
//  FrequenctView.swift
//  MicToFrequency
//
//  Created by owen on 2016/9/5.
//  Copyright © 2016年 Ampacs. All rights reserved.
//

import UIKit

class FrequenctView : UIView{
    fileprivate var barViews:[UIView] = []
    fileprivate var timer:Timer!
    fileprivate var interval:TimeInterval = 1/15
    
    var fftHelper:AMFFTHelper!
    
    override init(frame:CGRect){
        super.init(frame: frame)
        self.timer = Timer.scheduledTimer(timeInterval: self.interval, target: self, selector: #selector(self.onInterval), userInfo: nil, repeats: true)
        self.initializeViews()
    }
    
    required init?(coder aDecoder:NSCoder){
        super.init(coder: aDecoder)
        self.timer = Timer.scheduledTimer(timeInterval: self.interval, target: self, selector: #selector(self.onInterval), userInfo: nil, repeats: true)
        self.initializeViews()
    }
    
    deinit{
        self.timer.invalidate()
        self.timer = nil
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.updateBarFrames()
    }
    
    fileprivate func initializeViews(){
        self.initializeBars()
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    fileprivate func initializeBars(){
        for _ in 0..<AMFFTHelper.kLevelLength {
            let view = UIView(frame: CGRect.zero)
            view.backgroundColor = self.tintColor
            barViews.append(view)
            self.addSubview(view)
        }
    }
    
    fileprivate func updateBarFrames(){
        let viewHeight = self.frame.size.height
        let barWidth = self.frame.size.width / CGFloat(self.barViews.count)
        let barCount = self.barViews.count
        for i in 0..<barCount {
            let barView = self.barViews[i]
            var barHeight = CGFloat(0)
            
            if self.levels.count >= i {
                barHeight = viewHeight * CGFloat(self.levels[i])
            }
            
            barView.frame = CGRect(x: CGFloat(i)*barWidth, y: viewHeight - barHeight, width: barWidth, height: barHeight)
        }
        
    }
    
    var levels:Array<Float>=Array<Float>(repeating: 0, count: AMFFTHelper.kLevelLength)
    
    func onInterval(){
        if self.fftHelper != nil {
            for i in 0..<AMFFTHelper.kLevelLength {
                self.levels[i] = self.fftHelper.levels[i]
            }
        }
        self.updateBarFrames()
    }
}
