//
//  ViewController.swift
//  MicToFrequency
//
//  Created by owen on 2016/11/1.
//  Copyright © 2016年 Ampacs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var frequencyView: FrequenctView!
    
    fileprivate var recordHelper:AMRecordHelper!
    fileprivate var fftHelper:AMFFTHelper!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.recordHelper = AMRecordHelper()
        self.fftHelper = AMFFTHelper(self.recordHelper.bufferSize)
        self.recordHelper.fftHelper = self.fftHelper
        self.frequencyView.fftHelper = self.fftHelper
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func switchValueChanged(_ sender: UISwitch) {
        if self.recordHelper != nil {
            if sender.isOn {
                self.recordHelper.startAU()
            }else{
                self.recordHelper.stopAU()
            }
        }
    }

}

